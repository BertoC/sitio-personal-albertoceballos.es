<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--SEO==============================-->
    <title>Sitio personal de Alberto Ceballos, Diseñador/Desarrolador Web</title>
    <meta name="author" content="Alberto Ceballos <bertoceballos@hotmail.com>">
    <meta name="description" content="Bienvenido al sitio web de Alberto Ceballos, técnico superior en desarrollo de aplicaciones informáticas, desarrollador diseñador web, front-end back-end developer">
    <meta name="keywords" content="Alberto Ceballos,Diseño Web, Desarrollo Web,diseño web,desarrollo web,consultor IT,consultor web,diseñador web,desarrollador web,técnico informático,diseñador gráfico">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!--Librería de iconos "Font-Awesome"-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <!-- Estilos de efectos animate-->
    <link rel="stylesheet" href="animate.css" type="text/css">
    <!-- CSS principal-->
    <!--<link href="css-principal.less" rel="stylesheet/less" type="text/css">-->
    <link rel="stylesheet" type="text/css" href="css-principal.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="scripts/jquery.easing.1.3.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!--libreria JS para efecto de teclear:-->
    <script src="scripts/typeit.min.js"></script>
    <!--Script para el efecto de scroll de la página al clikar en los menús-->
    <script src="scripts/scroll-pagina.js" type="text/javascript"></script>
    <!--script para el menu flotante-->
    <script src="scripts/menu flotante.js" type="text/javascript"></script>
    <!--script para aparición de elementos al hacer scroll en la página-->
    <script src="scripts/WOW/dist/wow.js"></script>
    <!--script para la API de Google Maps-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAduRIKQQZQ81lat3OwH5M0uXuvi3gWBqA"></script>
    <script type="text/javascript" src="scripts/JSprincipal.js"></script>
</head>

<body>
    <?php
        // $_POST['lang'] = 'spanish'; 
        require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
    ?>

    <div id="loading">
        <img src="imagenes/Spin-1s-211px.gif">
    </div>
    <!-- Barra menú para dispositivos medios entre 576 y 992px-->
    <div class="menu-arriba"><span class="text-menu">Menú</span><img class="icono-menu" src="imagenes/menu.png"></div>
    <div id="contenido" class="container-fluid">
        <!-- //formulario  de idioma-->
        <form id="formIdioma" action="index.php" method="POST">
            <input id="inputIdioma" type="hidden" name="lang" value="" /> 
        </form>
        <div class="row">
            <!--Contenido barra lateral-->
            <aside class="col-12 col-sm-4 col-md-3 col-lg-2 barralateral">
                <div class="menu-flotante">
                    <div id="div-foto">
                        <img src="imagenes/foto.png" id="foto" class="rounded-circle img-fluid">
                    </div>
                    <ul class="nav flex-column" id="barnav">
                        <li><a href="#inicio" class="enlaces-menu"><?= $menu_home ?></a></li>
                        <li><a href="#sobreMi" class="enlaces-menu"><?= $menu_aboutMe ?></a></li>
                        <li><a href="#habilidades" class="enlaces-menu"><?= $menu_skills ?></a></li>
                        <li><a href="#formacion" class="enlaces-menu"><?= $menu_formation ?></a></li>
                        <li><a href="#experiencia" class="enlaces-menu"><?= $menu_experience ?></a></li>
                        <li><a href="#aptitudes" class="enlaces-menu"><?= $menu_aptitudes ?></a></li>
                        <li><a href="#contacto" class="enlaces-menu"><?= $menu_contact ?></a></li>
                        <li class="mt-4 d-flex justify-content-center li-idiomas">
                            <div class="div-li-social-element"> 
                                <span class="mr-4"><a  title="Idioma Español" class="enlace-idioma" href="#" id="enlaceIdiomaEspanol">ES</a></span>
                                <span><a title="English language" href="#" class="enlace-idioma" id="enlaceIdiomaIngles">EN</a></span>
                            </div>
                        </li>
                        <li class="mt-2 d-flex justify-content-center li-social">
                            <div class="div-social-content">
                            <div class="div-li-social-element" id="gitlab-social-div">
                                <a href="https://gitlab.com/albertoceballos" target="_blank" title="repositorio en Gitlab">
                                    <i class="fab fa-gitlab"></i>
                                </a>
                            </div>
                            <div class="div-li-social-element" id="linkedin-social-div">
                                <a href="https://www.linkedin.com/in/alberto-ceballos-guti%C3%A9rrez-284571140/" target="_blank" title="linkedIn personal" id="a-img-linkedin">
                                    <img id="img-linkedin" src="imagenes\LI-In-Bug.png"/> 
                                </a>
                            </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </aside>
            <!--Fin de la barra lateral-->
            <!--Inicio contenido principal-->
            <div class="col-12 col-sm-8 offset-sm-4 col-md-9 offset-md-3 col-lg-10 offset-lg-2 main main-small">
                <section id="inicio">
                    <!--carrusel con las imágenes de fondo en el background de los div-->
                    <div id="carrusel-inicio" class="carousel slide carousel-fade" data-ride="carousel" data-interval="4000">
                        <ol class="carousel-indicators">
                            <li data-target="carrusel-inicio" data-slide-to="0" class="active"></li>
                            <li data-target="carrusel-inicio" data-slide-to="1"></li>
                            <li data-target="carrusel-inicio" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div id="div-hola">
                                <span class="div-hola-subtitulo"><?= $bienvenida_subtitle ?></span>
                                <br>
                                <div id="mascara-hola">ALBERTO CEBALLOS</div>
                                <div id="div-type" class="div-hola-subtitulo"></div>
                                <div class="descarga"><a class="btn btn-primary btn-descarga" href="docs/CV Alberto Ceballos Gutiérrez.pdf"><?= $download_CV ?><i class="fa fa-file-pdf-o"></i></a></div>
                            </div>
                            <div class="carousel-item active divs-carrusel carrusel1">
                            </div>
                            <div class="carousel-item divs-carrusel carrusel2">
                            </div>
                            <div class="carousel-item divs-carrusel carrusel3">
                            </div>
                        </div>
                    </div>
                </section>
                <section id="sobreMi" class="wow fadeInUpBig">
                    <div class="row justify-content-center">
                        <div class="cabeza-section"></div>
                        <div class="titulo-section"><?= $aboutMe_title ?></div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-3 div-foto-sobremi wow zoomInDown">
                            <img src="imagenes/foto_perfil_grande.jpg" alt="foto">
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-5 info-bloque wow fadeInUpBig">
                                    <div class="info-icon azul"><i class="far fa-user"></i></div>
                                    <div class="info-text"><span class="info-head-text"><?= $aboutMe_name ?></span>
                                        <br>Alberto Ceballos Gutiérrez</div>
                                </div>
                                <div class="col-md-7 info-bloque wow fadeInUpBig">
                                    <div class="info-icon rojo"><a href="mailto:bertoceballos@hotmail.com>"<i class="far fa-envelope"></i></a></div>
                                    <div class="info-text"><span class="info-head-text"><?= $aboutMe_email ?></span>
                                        <br><a href="mailto:bertoceballos@hotmail.com">bertoceballos@hotmail.com</a></div>
                                </div>
                                <div class="col-md-5 info-bloque wow fadeInUpBig">
                                    <div class="info-icon verde"><i class="fab fa-whatsapp whatsapp"></i></div>
                                    <div class="info-text"><span class="info-head-text"><?= $aboutMe_phone ?></span>
                                        <br>626724601</div>
                                </div>
                                <div class="col-md-7 info-bloque wow fadeInUpBig">
                                    <div class="info-icon azuloscuro"><i class="far fa-calendar-alt calendario"></i></div>
                                    <div class="info-text"><span class="info-head-text"><?= $aboutMe_birthDate ?></span>
                                        <br>20-09-1983</div>
                                </div>
                                <div class="col-md-5 info-bloque wow fadeInUpBig">
                                    <div class="info-icon azul domicilio"><i class="fas fa-map-marker-alt"></i></div>
                                    <div class="info-text"><span class="info-head-text"><?= $aboutMe_address ?></span>
                                        <br>Santander(Cantabria)</div>
                                </div>
                                <div class="col-md-7 info-bloque wow fadeInUpBig">
                                    <div class="info-icon rojo"><i class="far fa-flag"></i></div>
                                    <div class="info-text"><span class="info-head-text"><?= $aboutMe_nationality ?></span>
                                        <br><?= $aboutMe_spa ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 wow zoomIn">
                            <p><?= $aboutMe_pa1 ?></p>
                            <p><?= $aboutMe_pa2 ?></p>
                            <p><?= $aboutMe_pa3 ?></p>
                            <p><?= $aboutMe_pa4 ?></p>
                            <p><?= $aboutMe_pa5 ?></p>
                        </div>
                    </div>
                </section>
                <!--Fin de la sección "Sobre Mi" e inicio de la sección "Habilidades"-->
                <section id="habilidades" class="wow fadeInUpBig">
                    <div class="row justify-content-center">
                        <div class="cabeza-section"></div>
                        <div class="titulo-section"><?= $skills_title ?></div>
                    </div>
                    <!--Barras de progreso de las habilidades digitales-->
                    <div class="row justify-content-center">
                        <div class="col-sm-3 bloque-barra">
                            <div class="porcentaje tooltip-verde tool90 wow fadeInLeft" data-wow-delay="1.5s">90%</div>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped barra-verde wow fadeInLeft" data-wow-delay="1s" data-wow-duration="2.5s" role="progressbar" style="width: 90%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" id="barra1">
                                </div>
                            </div>HTML 5
                        </div>
                        <div class="col-sm-3 bloque-barra inline">
                            <span class="porcentaje tooltip-azul tool80 wow fadeInLeft" data-wow-delay="1.0s">80%</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped barra-azul wow fadeInLeft" data-wow-delay="1s" data-wow-duration="2.5s" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" id="barra1"></div>
                            </div>GIMP
                        </div>
                        <div class="col-sm-3 bloque-barra inline">
                            <span class="porcentaje tooltip-rojo tool70 wow fadeInLeft" data-wow-delay="1.0s">70%</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped barra-rojo wow fadeInLeft" data-wow-delay="1s" data-wow-duration="1.5s" role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" id="barra1"></div>
                            </div>Photoshop
                        </div>
                        <div class="col-sm-3 bloque-barra inline">
                            <span class="porcentaje tooltip-verde tool40 wow fadeInLeft" data-wow-delay="1.0s">40%</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped barra-verde wow fadeInLeft" data-wow-delay="1s" data-wow-duration="1.5s" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" id="barra1"></div>
                            </div> Visual Basic
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-sm-3 bloque-barra">
                            <span class="porcentaje tooltip-verde tool85 wow fadeInLeft" data-wow-delay="1.0s">85%</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped barra-verde wow fadeInLeft" data-wow-delay="1s" data-wow-duration="1.5s" role="progressbar" style="width: 85%" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" id="barra1"></div>
                            </div> CSS 3/Bootstrap/Sass
                        </div>
                        <div class="col-sm-3 bloque-barra">
                            <span class="porcentaje tooltip-azul tool70 wow fadeInLeft" data-wow-delay="1.0s">70%</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped barra-azul wow fadeInLeft" data-wow-delay="1s" data-wow-duration="2.5s" role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" id="barra1"></div>
                            </div>PHP
                        </div>
                        <div class="col-sm-3 bloque-barra inline">
                            <span class="porcentaje tooltip-rojo tool60 wow fadeInLeft" data-wow-delay="1.0s">60%</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped barra-rojo wow fadeInLeft" data-wow-delay="1s" data-wow-duration="2.5s" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" id="barra1"></div>
                            </div>Angular
                        </div>
                        <div class="col-sm-3 bloque-barra inline">
                            <span class="porcentaje tooltip-verde tool40 wow fadeInLeft" data-wow-delay="1.0s">40%</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped barra-verde wow fadeInLeft" data-wow-delay="1s" data-wow-duration="2.5s" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" id="barra1"></div>
                            </div>C/C++
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-sm-3 bloque-barra">
                            <span class="porcentaje tooltip-verde tool80 wow fadeInLeft" data-wow-delay="1.0s">80%</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped barra-verde wow fadeInLeft" data-wow-delay="1s" data-wow-duration="1.5s" role="progressbar" style="width: 80%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" id="barra1"></div>
                            </div>Javascript/Typescript
                        </div>
                        <div class="col-sm-3 bloque-barra">
                            <span class="porcentaje tooltip-azul tool60 wow fadeInLeft" data-wow-delay="1.0s">60%</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped barra-azul wow fadeInLeft" data-wow-delay="1s" data-wow-duration="1.5s" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" id="barra1"></div>
                            </div>Mysql
                        </div>
                        <div class="col-sm-3 bloque-barra inline">
                            <span class="porcentaje tooltip-rojo tool40 wow fadeInLeft" data-wow-delay="1.0s">40%</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped barra-rojo wow fadeInLeft" data-wow-delay="1.0s" data-wow-duration="1.5s" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" id="barra1"></div>
                            </div>ActionScript
                        </div>
                        <div class="col-sm-3 bloque-barra inline">
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-sm-3 bloque-barra">
                            <span class="porcentaje tooltip-verde tool80 wow fadeInLeft" data-wow-delay="1.0s">80%</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped barra-verde wow fadeInLeft" data-wow-delay="1.0s" data-wow-duration="2.5s" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" id="barra1"></div>
                            </div>JQuery
                        </div>
                        <div class="col-sm-3 bloque-barra">
                            <span class="porcentaje tooltip-azul tool80 wow fadeInLeft" data-wow-delay="1.0s">80%</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped barra-azul wow fadeInLeft" data-wow-delay="1.0s" data-wow-duration="2.5s" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" id="barra1"></div>
                            </div>Git
                        </div>
                        <div class="col-sm-3 bloque-barra inline">
                            <span class="porcentaje tooltip-rojo tool75 wow fadeInLeft" data-wow-delay="1.0s">75%</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped barra-rojo wow fadeInLeft" data-wow-delay="1.0s" data-wow-duration="2.5s" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" id="barra1"></div>
                            </div>Apache/PhpMyAdmin
                        </div>
                        <div class="col-sm-3 bloque-barra inline">
                        </div>
                    </div>
                    <!--Idiomas-->
                    <div class="row justify-content-center">
                        <div class="col-sm-12 subtitulo">
                            <?= $language_skills ?>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-sm-6 d-flex flex-column justify-content-center align-items-center idiomas">
                            <div class="fondoGB  wow fadeInUp" data-wow-delay="1s"></div>
                            <div class="cruza1  wow fadeInRight" data-wow-delay="1s"></div>
                            <div class="cruza2 wow fadeInLeft" data-wow-delay="1s"></div>
                            <img class="GB wow zoomIn" data-wow-delay="1s" src="imagenes/Gran Bretaña.png">
                            <div class="texto-idiomas"><?= $english_level ?></div>
                        </div>
                        <div class="col-sm-6 d-flex flex-column justify-content-center align-items-center idiomas">
                            <div class="francia_fondo wow fadeInUp" data-wow-delay="1s"></div>
                            <div class="francia_azul wow fadeInDown" data-wow-delay="1s"></div>
                            <img class="francia wow zoomIn" data-wow-delay="1s" src="imagenes/francia.png">
                            <div class="texto-idiomas"><?= $french_level ?></div>
                        </div>
                    </div>
                </section>
                <!--Fin de la sección "Habilidades" e inicio de la sección "Formación"-->
                <section id="formacion" class="wow fadeInUpBig">
                    <div class="row justify-content-center">
                        <div class="cabeza-section"></div>
                        <div class="titulo-section"><?= $training_title ?></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 d-flex justify-content-center">
                            <div class="barra-estudios"></div>
                            <div class="estudios estudios-izquierda estudios1 wow zoomInUp">
                                <span class="font-weight-bold"><?= $training_name1 ?></span> 
                                <br>
                                <span><?= $training_desc1 ?></span>
                            </div>
                            <div class="estudios estudios-derecha estudios2 wow zoomInUp">
                                <span class="font-weight-bold"><?= $training_name2 ?></span>
                                <span><?= $training_desc2 ?></span>
                            </div>
                            <div class="estudios estudios-izquierda estudios3 wow zoomInUp">
                                <span class="font-weight-bold"><?= $training_name3 ?></span>
                                <span><?= $training_desc3 ?></span>
                            </div>
                            <div class="estudios estudios-derecha estudios4 wow zoomInUp">
                                <span class="font-weight-bold"><?= $training_name4 ?></span>
                                <span><?= $training_desc4 ?></span>
                            </div>
                            <div class="estudios estudios-izquierda estudios5 wow zoomInUp">
                                <span class="font-weight-bold"><?= $training_name5 ?></span> 
                                <span><?= $training_desc5 ?></span>
                            </div>
                        </div>
                    </div>
                </section>
                <!--Fin de la sección "Formación" e inicio de la sección "Experiencia"-->
                <section id="experiencia" class="wow fadeInUp">
                    <div class="row justify-content-center">
                        <div class="cabeza-section"></div>
                        <div class="titulo-section"><?= $experience_title ?></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 d-flex justify-content-center">
                            <div class="barra-experiencia"></div>
                            <div class="experiencia experiencia-izquierda experiencia1 wow zoomInDown">
                                <span class="font-weight-bold"><?= $experience_name1 ?></span>
                                <br>
                                <span> <?= $experience_desc1 ?></span>
                            </div>
                            <div class="experiencia experiencia-derecha experiencia2 wow zoomInDown">
                                <span class="font-weight-bold"><?= $experience_name2 ?></span>
                                <br>
                                <span><?= $experience_desc2 ?></span>
                            </div>
                            <div class="experiencia experiencia-izquierda experiencia3 wow zoomInDown">
                                <span class="font-weight-bold"><?= $experience_name3 ?></span>
                                <br>
                                <span><?= $experience_desc3 ?></span>
                            </div>
                            <div class="experiencia experiencia-derecha-grande <?= $currently=='es' ? 'experiencia4_es' : 'experiencia4_en' ?> wow zoomInDown">
                                <span class="font-weight-bold"><?= $experience_name4 ?></span>
                                <br>
                                <span><?= $experience_desc4 ?></span>
                            </div>
                        </div>
                </section>
                <!--Fin de la sección Experiencia e inicio de la sección Aptitudes -->
                <section id="aptitudes" class="wow fadeInUp">
                    <div class="row justify-content-center">
                        <div class="cabeza-section"></div>
                        <div class="titulo-section"><?= $aptitudes_title ?></div>
                    </div>
                    <div class="row justify-content-around">
                        <div class="col-md-2 wow flipInX columna-aptitudes d-flex justify-content-center flex-column align-items-center" data-wow-delay="1.0s">
                            <div class="aptitud"><i class="fas fa-code aptitud1"></i></div>
                            <div id="aptitud_text1" class="aptitud_text"><?= $aptitudes_name1 ?></div>
                        </div>
                        <div class="col-md-2 wow bounceIn columna-aptitudes d-flex justify-content-center flex-column align-items-center" data-wow-delay="1.0s">
                            <div class="aptitud"><i class="fas fa-paint-brush aptitud2"></i></div>
                            <div id="aptitud_text2" class="aptitud_text"><?= $aptitudes_name2 ?></div>
                        </div>
                        <div class="col-md-2 wow bounceIn columna-aptitudes d-flex justify-content-center flex-column align-items-center" data-wow-delay="1.0s">
                            <div class="aptitud"><i class="fas fa-suitcase aptitud3"></i></div>
                            <div id="aptitud_text3" class="aptitud_text"><?= $aptitudes_name3 ?></div>
                        </div>
                        <div class="col-md-2 wow flipInY columna-aptitudes d-flex justify-content-center flex-column align-items-center" data-wow-delay="1.0s">
                            <div class="aptitud"><i class="fas fa-bullseye aptitud4"></i></div>
                            <div id="aptitud_text4" class="aptitud_text"><?= $aptitudes_name4 ?></div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12 subtitulo">
                            <?= $hobby_title ?>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-2 wow zoomIn columna-aptitudes d-flex justify-content-center flex-column align-items-center">
                            <div class="aptitud"><i class="fas fa-laptop aptitud5"></i></div>
                            <div id="aptitud_text5" class="aptitud_text"><?= $hobby_name1 ?></div>
                        </div>
                        <div class="col-md-2 wow bounceIn columna-aptitudes d-flex justify-content-center flex-column align-items-center">
                            <div class="aptitud"><i class="fas fa-book aptitud6"></i></div>
                            <div id="aptitud_text6" class="aptitud_text"><?= $hobby_name2 ?></div>
                        </div>
                        <div class="col-md-2 wow bounceIn columna-aptitudes d-flex justify-content-center flex-column align-items-center">
                            <div class="aptitud"><i class="fas fa-music aptitud7"></i></div>
                            <div id="aptitud_text7" class="aptitud_text"><?= $hobby_name3 ?></div>
                        </div>
                        <div class="col-md-2 wow zoomIn columna-aptitudes d-flex justify-content-center flex-column align-items-center">
                            <div class="aptitud"><i class="fas fa-gamepad aptitud8"></i></div>
                            <div id="aptitud_text8" class="aptitud_text"><?= $hobby_name4 ?></div>
                        </div>
                    </div>
                </section>
                <!--********** Fin de la sección Aptitudes/ Inicio de la sección Contacto**************-->
                <section id="contacto" class="wow fadeInUp">
                    <div class="row justify-content-center">
                        <div class="cabeza-section"></div>
                        <div class="titulo-section"><?= $contact_title ?></div>
                    </div>
                    <div class="row justify-content-center wow fadeIn" data-wow-delay="0.5s">
                        <div style="width: 100%"><iframe width="100%" height="500" src="<?= $google_maps_src ?>" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.mapsdirections.info/crear-un-mapa-de-google/">crear mapa Google Maps</a> by <a href="https://www.mapsdirections.info/">Mapa España</a></iframe></div><br/>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-sm-6">
                            <form id="formContacto" name="formContacto" method="post" action="contacto.php">
                                <div id="respuestaForm"></div>
                                <div class="form-group">
                                    <label for="nombre"><?= $contact_name ?></label>
                                    <input type="text" id="form-nombre" name="nombre" class="form-control formulario">
                                </div>
                                <div class="form-group">
                                    <label for="email"><?= $contact_email ?></label>
                                    <input type="text" id="form-email" name="email" class="form-control formulario">
                                </div>
                                <div class="form-group">
                                    <label for="asunto"><?= $contact_subject ?></label>
                                    <input type="text" id="form-asunto" name="asunto" class="form-control formulario">
                                </div>
                                <div class="form-group">
                                    <label for="telefono"><?= $contact_phone ?></label>
                                    <input type="tel" id="form-tel" name="telefono" class="form-control formulario">
                                </div>
                                <div class="form-group">
                                    <label for="mensaje"><?= $contact_message ?></label>
                                    <textarea id="form-mensaje" name="mensaje" class="form-control formulario"></textarea>
                                </div>
                                <input type="submit" id="form-enviar" name="enviar" class="btn btn-primary btn-descarga mb-4" value="<?= $contact_submit ?>">
                            </form>
                        </div>
                    </div>
                </section>
                </div>
        </div>
             <!--back to top, div en la esquina derecha para volver arriba-->
            <div id="back-top"><i class="far fa-arrow-alt-circle-up"></i></div>
    </div>
    <!--Fin container-->
</body>

</html>