<?php
    if(isset($_POST['lang']) && $_POST['lang']=='english')
    {
        require_once($_SERVER['DOCUMENT_ROOT'].'/locale/en_GB/en_GB.php');
        echo "<script src='scripts/type_it_eng.js'></script>";
    }
    else
    {
        require_once($_SERVER['DOCUMENT_ROOT'].'/locale/es_ES/es_ES.php');
        echo "<script src='scripts/type_it_esp.js'></script>";
    }
?>