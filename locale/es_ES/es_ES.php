<?php
    //Menu lateral
    $menu_home = "INICIO";
    $menu_aboutMe = "SOBRE MI";
    $menu_skills="HABILIDADES";
    $menu_formation = "FORMACIÓN";
    $menu_experience = "EXPERIENCIA";
    $menu_aptitudes = "APTITUDES";
    $menu_contact = "CONTACTO";

    //Bienvenida
    $bienvenida_subtitle = "Bienvenidos, soy";

    //botón descarga C.V
    $download_CV ="Descargar mi C.V ";

    //Sección "SOBRE MI"
    $aboutMe_title = "Sobre Mí";
    $aboutMe_name = "Nombre:";
    $aboutMe_email = "Email:";
    $aboutMe_phone = "Teléfono:";
    $aboutMe_birthDate = "Fecha Nacimiento:";
    $aboutMe_address = "Domicilio:";
    $aboutMe_nationality = "Nacionalidad:";
    $aboutMe_spa = "Española:";
    $aboutMe_pa1 = "Soy Técnico Superior en desarrollo de aplicaciones informáticas, con formación adicional y experiencia centrada en el desarrollo de aplicaciones web ya que es la especialidad que realmente me atrae. Me gustaría actualizar, ampliar conocimientos y sobre todo adquirir más experiencia para desarrollar a un mayor nivel mi carrera.";
    $aboutMe_pa2 = "A nivel formativo aparte de mi titulación principal, he cursado prácticas cómo desarrollador web y cursos por más de 1500 horas vinculados con el diseño, desarrollo y administración web, destacando el certificado de profesionalidad de nivel III en desarrollo de aplicaciones web.";
    $aboutMe_pa3 = "Como experiencia profesional destacaría mi reciente trabajo como desarrollador web junior en Sopra Steria trabajando para un importante cliente bancario en varios proyectos en entorno XAMPP mediante CakePHP, PostgreSQL, JS/Jquery y CSS con uso de Bootstrap 4. También como desarrollador frontend de Angular en proyecto API-RESTful para la Consejería Educación del Gobierno de Castilla Y León. Además, realicé tareas de testing mediante Cypress.";
    $aboutMe_pa4 = "Actualmente ejerzo como desarrollador web en el grupo Zemsania Global Tech desde Diciembre de 2021. Mis tareas aquí hasta la fecha son: Validación de accesibilidad web mediante uso de análisis de código estático con Kiuwan para proyectos web de la administración pública desarrollados con Java. Uso de HTML, CSS, Js/Jquery, Java. También  tareas de integración continua mediante Jenkins.";
    $aboutMe_pa5 ="En mi repositorio en Gitlab puden encontrar proyectos personales con uso de estas y otras tecnologías. Actualmente, además de mi trabajo profesional sigo aprendiendo desarrollo web mediante cursos online en Udemy.com.";

    //Sección Habilidades
    $skills_title = "Habilidades";
    $language_skills = "Conocimientos de idiomas";
    $english_level = "Inglés: Nivel Medio";
    $french_level = "Francés: Nivel Bajo";

    //Seccion Formación
    $training_title = "Formación";
    $training_name1 = "Grado Superior en Desarrollo de aplicaciones informáticas";
    $training_desc1 = "Centro concertado Cedefor";
    $training_name2 = "Curso diseño de páginas web";
    $training_desc2 = "Formacíon ocupacional";
    $training_name3 = "Curso administrador de redes";
    $training_desc3 = "Formacíon ocupacional";
    $training_name4 = "Curso administrador servidores y páginas web";
    $training_desc4 = "Formacíon ocupacional";
    $training_name5 = "Desarrollo aplicaciones con tecnologías web";
    $training_desc5 = "Certificado profesionalidad (Nivel III)";

    //Sección Experiencia
    $experience_title = "Experiencia";
    $experience_name1 = "Maquetador web. Fraile & Blanco";
    $experience_desc1 = "Practicas formativas como maquetador web";
    $experience_name2 = "Unipost S.A Servicio postal nacional";
    $experience_desc2 = "Clasificación/reparto de correspondencia";
    $experience_name3 = "Desarrollador web. Sopra Steria";
    $experience_desc3 = "Desarrollo web en PHP(CakePHP), JS, JQuery,Angular";
    $experience_name4 = "Desarrollador web. Zemsania Global Tech";
    $experience_desc4 = "Validación de accesibilidad web mediante uso de análisis de código estático con Kiuwan para proyectos web de la administración pública desarrollados con Java. Uso de HTML, CSS, Js. Integración continua con Jenkins";
    $currently = "es";
    //Seccion Aptitudes
    $aptitudes_title = "Aptitudes";
    $aptitudes_name1 = "Autodidacta";
    $aptitudes_name2 = "Creatividad";
    $aptitudes_name3 = "Responsable";
    $aptitudes_name4 = "Persistencia";

    $hobby_title = "aficiones";
    $hobby_name1 = "Informática/Programación";
    $hobby_name2 = "Lectura";
    $hobby_name3 = "Música";
    $hobby_name4 = "Videojuegos";

    //Sección Contacto
    $contact_title = "Contacto";
    $contact_name = "Nombre";
    $contact_email = "E-Mail";
    $contact_subject = "Asunto";
    $contact_phone = "Teléfono";
    $contact_message = "Mensaje";
    $contact_submit = "Enviar mensaje";
    $google_maps_src = "https://maps.google.com/maps?width=100%&height=600&hl=es&q=floranes%208%2B+(Mi%20Alberto%20de%20Ceballos)&ie=UTF8&t=h&z=14&iwloc=B&output=embed";

?>