<?php
    //Menu lateral
    $menu_home = "HOME";
    $menu_aboutMe = "ABOUT ME";
    $menu_skills="SKILLS";
    $menu_formation = "STUDIES";
    $menu_experience = "EXPERIENCE";
    $menu_aptitudes = "APTITUDES";
    $menu_contact = "CONTACT";

    //Bienvenida
    $bienvenida_subtitle = "Welcome, I am";

    //botón descarga C.V
    $download_CV ="Download my C.V ";

    //Sección "SOBRE MI"
    $aboutMe_title = "About me";
    $aboutMe_name = "Name:";
    $aboutMe_email = "Email:";
    $aboutMe_phone = "Phone:";
    $aboutMe_birthDate = "Birth Date:";
    $aboutMe_address = "Address:";
    $aboutMe_nationality = "Nacionality:";
    $aboutMe_spa = "Spanish:";
    $aboutMe_pa1 = "I am a Advanced Technician in the development of computer applications, with additional training and experience focused on the development of web applications, since it is the specialty that really attracts me. I would like to update, expand my knowledge and above all acquire more experience to develop my career to a higher level.";
    $aboutMe_pa2 = "At education level apart from my main degree, I have completed internships as web developer and courses for more than 1500 hours related to web design, development and administration, highlighting the level III professional certificate in web application development.";
    $aboutMe_pa3 = "As professional experience, I would highlight my recent work as junior web developer at Sopra Steria working for an important banking client on several projects in a XAMPP environment using CakePHP, PostgreSQL, JS/Jquery and CSS using Bootstrap 4. Also as Angular frontend developer on API-RESTful project for the Ministry of Education of the Government of Castilla Y León. In addition, I performed testing tasks using Cypress.";
    $aboutMe_pa4 = "I currently work as a web developer in Zemsania Global Tech group since December 2021. My tasks here to date are: Validation of web accessibility using static code analysis with Kiuwan for public administration web projects developed with Java. Use of HTML, CSS, Js/Jquery, Java. Also continuous integration tasks using Jenkins.";
    $aboutMe_pa5 = "In my Gitlab repository you can find personal projects using these and other technologies. Currently, in addition to my professional work, I continue to learn web development through online courses at Udemy.com.";

    //Sección Habilidades
    $skills_title = "Skills";
    $language_skills = "Language skills";
    $english_level = "English: Medium level";
    $french_level = "French: Low level";

    //Seccion Formación
    $training_title = "Training";
    $training_name1 = "Superior Degree in Computer Application Development";
    $training_desc1 = "Cedefor concerted center";
    $training_name2 = "Web page design course";
    $training_desc2 = "Ocupational training";
    $training_name3 = "Computer network administration course";
    $training_desc3 = "Ocupational training";
    $training_name4 = "Server and website administration course";
    $training_desc4 = "Ocupational training";
    $training_name5 = "Web applications development";
    $training_desc5 = "Professional certificate (Level III)";

    //Sección Experiencia
    $experience_title = "Experience";
    $experience_name1 = "Web developer. Fraile & Blanco";
    $experience_desc1 = "Training practices as web developer";
    $experience_name2 = "Unipost S.A National postal service";
    $experience_desc2 = "Mail delivery/clasiffication";
    $experience_name3 = "Web developer. Sopra Steria";
    $experience_desc3 = "Web development using PHP(CakePHP), JS, JQuery,Angular";
    $experience_name4 = "Web developer. Zemsania Global Tech";
    $experience_desc4 = "Validation of web accessibility through the use of static code analysis with Kiuwan for public administration web projects developed with Java. Use of HTML, CSS, Js. Continuous integration with Jenkins";
    $currently = "en";
    //Seccion Aptitudes
    $aptitudes_title = "Aptitudes";
    $aptitudes_name1 = "Autodidact";
    $aptitudes_name2 = "Creativity";
    $aptitudes_name3 = "Responsible";
    $aptitudes_name4 = "Persistence";

    $hobby_title = "hobbies";
    $hobby_name1 = "Computing/programming";
    $hobby_name2 = "Reading";
    $hobby_name3 = "Music";
    $hobby_name4 = "Videogames";

    //Sección Contacto
    $contact_title = "Contact";
    $contact_name = "Name";
    $contact_email = "E-Mail";
    $contact_subject = "Subject";
    $contact_phone = "Phone";
    $contact_message = "Message";
    $contact_submit = "Send message";
    $google_maps_src = "https://maps.google.com/maps?width=100%&height=600&hl=en&q=floranes%208%2B+(Mi%20Alberto%20de%20Ceballos)&ie=UTF8&t=h&z=14&iwloc=B&output=embed";


?>