//Script para la función de TypeIT del efecto de tecleear

window.addEventListener('load',() => {
    var instance = new TypeIt('#div-type', {
        speed: 150,
    }).type('Web Designer/').break().type('Developer').pause('1500').delete().type('Front-End/Back-End Developer')
        .delete(28).type('Full-Stack Developer');
});