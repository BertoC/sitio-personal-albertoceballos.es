$("document").ready(function(){
  var FloatingMenuApp = {
    $options: {
      float_speed: 1500,
      float_easing: 'easeOutQuint',
      menu_fade_speed: 500,
      closed_menu_opacity: 0.75
    },
    $flmenu: $(".barralateral"),
    $sidebarheader: $(".menu-flotante"),
    $menuPosition: 0,
    floatMenu: function () {
      var scrollAmount = $(document).scrollTop();
      var newPosition = this.$menuPosition + scrollAmount;
      if ($(window).height() < this.$flmenu.height()) {
        this.$flmenu.css("top", this.$menuPosition);
      } else {
        if (newPosition <= 0) newPosition = 0
        this.$flmenu.stop().animate({
          top: newPosition
        }, this.$options.float_speed, this.$options.float_easing);
      }
    },
    initMenuOnLoad: function () {
      this.$menuPosition = this.$flmenu.position().top;
      this.floatMenu();
    },
    scrollMenu: function () {
      if ($(document).width() >= 768) this.floatMenu();
    },
    bindEvents: function () {},
    init: function (_options) {
      console.log();
      $.extend(this.$options, _options);
      this.bindEvents();
      return this;
    }
  };
  var App = {
    $options: {},
    $floatMenu: FloatingMenuApp.init(),
    /*$navbarVertical: $('.navbar-vertical'),*/
    /*$menuItem: $('#barnav a'),*/
    bindEvents: function () {
      $(window).on('load', this.load.bind(this));
      $(window).on('scroll', this.scroll.bind(this));
    },
    load: function () {
      this.$floatMenu.initMenuOnLoad();
    },
    scroll: function () {
      this.$floatMenu.scrollMenu();
    },
    init: function (_options) {
      $.extend(this.$options, _options);
      this.bindEvents();
    }
  };
  App.init({});
});