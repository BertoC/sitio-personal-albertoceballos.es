//Script para la función de TypeIT del efecto de tecleear

window.addEventListener('load',()=>{
    var instance = new TypeIt('#div-type', {
        speed: 150,
    }).type('Diseñador/').break().type('Desarrollador Web').pause('1500').delete().type('Desarrollador Front-End/Back-End')
    .delete(32).type('Desarrollador Full-Stack');
});
